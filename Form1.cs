using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Programming3
{
    public partial class statsForm : Form
    {
        public statsForm()
        {
            InitializeComponent();
        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void openFileButton_Click(object sender,EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                String readText;
                String[] currentWord;
                String firstWordAlphabetically = "";
                String lastWordAlphabetically = "";
                String longestWord = "";
                String mostVowels = "";
                char[] wordCharArray;
                int currentVowelCount;
                int mostVowelCount = 0;

                statsListBox.Items.Clear();

                try
                {
                    StreamReader textFile = new StreamReader(openFileDialog.FileName);
                    StreamWriter saveFile = File.CreateText("Statistics.txt");

                    while (!textFile.EndOfStream)
                    {
                        readText = textFile.ReadLine();
                        readText = readText.ToLower();
                        currentWord = readText.Split(' ');

                        for (int i = 0; i < currentWord.Length; i++)
                        {
                            currentVowelCount = 0;
                            wordCharArray = currentWord[i].ToCharArray();

                            if(firstWordAlphabetically == "")
                            {
                                firstWordAlphabetically = currentWord[i];
                            }

                            if (lastWordAlphabetically == "")
                            {
                                lastWordAlphabetically = currentWord[i];
                            }

                            else if (currentWord[i].CompareTo(lastWordAlphabetically) > 0)
                            {
                                lastWordAlphabetically = currentWord[i];
                            }

                            if (longestWord == "")
                            {
                                longestWord = currentWord[i];
                            }

                            else if (longestWord.Length < currentWord[i].Length)
                            {
                                longestWord = currentWord[i];
                            }

                            for(int j = 0; j < wordCharArray.Length; j++)
                            {
                                if(wordCharArray[j] == 'a' ||
                                    wordCharArray[j] == 'e' ||
                                    wordCharArray[j] == 'i' ||
                                    wordCharArray[j] == 'o' ||
                                    wordCharArray[j] == 'u')
                                {
                                    currentVowelCount++;
                                }
                            }

                            if(currentVowelCount > mostVowelCount)
                            {
                                mostVowelCount = currentVowelCount;
                                mostVowels = currentWord[i];
                            }
                        }
                    }

                    statsListBox.Items.Add("First word alphabetically: " + firstWordAlphabetically);
                    statsListBox.Items.Add("Last word alphabetically: " + lastWordAlphabetically);
                    statsListBox.Items.Add("Longest word: " + longestWord + " with " + longestWord.Length + " characters.");
                    statsListBox.Items.Add("Word with the most vowels: " + mostVowels + " with " + mostVowelCount + " vowels.");

                    saveFile.WriteLine("First word alphabetically: " + firstWordAlphabetically);
                    saveFile.WriteLine("Last word alphabetically: " + lastWordAlphabetically);
                    saveFile.WriteLine("Longest word: " + longestWord + " with " + longestWord.Length + " characters.");
                    saveFile.WriteLine("Word with the most vowel: " + mostVowels + " with " + mostVowelCount + " vowels.");

                    saveFile.Close();
                    textFile.Close();
                }

                catch(Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }
    }
}
